﻿using System;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SwqaCloud.Pages
{
    class CreatePage : BaseApplicationPage
    {
        public RunEnvironment RunEnv { get; } = new RunEnvironment();

        public CreatePage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            RunEnv.Url = user.RunEnviroment == "PROD" ? Url.Production.LoginPage : Url.Test.LoginPage;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        internal void FillOutNewClientForm(Users.CurrentUser newUser)
        {
            EnterClientName(newUser);
            EnterLocation(newUser);
            EnterUserName(newUser);
            SelectStatus(newUser);
            EnterPassword(newUser);
            EnterPostalAddress(newUser);
            EnterMobilePhone(newUser);
            EnterSMSNumber(newUser);
            SelectFirstMessengerService(newUser);
            EnterFirstMessengerUserName(newUser);
            SelectSecondMessengerService(newUser);
            EnterSecondMessengerUserName(newUser);
            EnterEmailAddress(newUser);
            EnterLikedInURL(newUser);
            EnterResumeURL(newUser);
            // assert client was added 
            // cleanup i.e. removing the client
        }

        private void EnterResumeURL(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.ResumeURL);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.ResumeURL)).SendKeys(newUser.ResumeURL);
            Reporter.LogPassingTestStepToBugLogger($"Put in the Reume URL into the resume input field. => {newUser.ResumeURL}");
        }

        private void EnterLikedInURL(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.LinkedInURL);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.LinkedInURL)).SendKeys(newUser.LinkedInURL);
            Reporter.LogPassingTestStepToBugLogger($"Put in the LinkedIn URL into the LinkedIn input field. => {newUser.LinkedInURL}");
        }

        private void EnterEmailAddress(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.EmailAddress);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.EmailAddress)).SendKeys(newUser.EmailAddress);
            Reporter.LogPassingTestStepToBugLogger($"Put in the email address in the email input field. => {newUser.EmailAddress}");
        }

        private void EnterSecondMessengerUserName(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.MessengerId_2);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.MessengerId_2)).SendKeys(newUser.MessengerId_2);
            Reporter.LogPassingTestStepToBugLogger($"Put in the messenger user name for the second messinger service. => {newUser.MessengerId_2}");
        }

        private void SelectSecondMessengerService(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Dropdown.IMSelect_2);
            DropdownSelect(Elements.CreatePage.Dropdown.IMSelect_2, Elements.CreatePage.Dropdown.Viber_2);
            Reporter.LogPassingTestStepToBugLogger($"Select the IM service from the Second IM dropdown list. => {newUser.IM_02_Service}");
        }

        private void EnterFirstMessengerUserName(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.MessengerId_1);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.MessengerId_1)).SendKeys(newUser.MessengerId_1);
            Reporter.LogPassingTestStepToBugLogger($"Put in the messenger user name for the first messinger service. => {newUser.MessengerId_1}");
        }

        private void SelectFirstMessengerService(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Dropdown.IMSelect_1);
            DropdownSelect(Elements.CreatePage.Dropdown.IMSelect_1, Elements.CreatePage.Dropdown.Skype_1);
            Reporter.LogPassingTestStepToBugLogger($"Select the IM service from the first IM dropdown list. => {newUser.IM_01_Service}");
        }

        private void EnterSMSNumber(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.SMS);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.SMS)).SendKeys(newUser.SMS);
            Reporter.LogPassingTestStepToBugLogger($"Put in the SMS phone number into the SMS input field. => {newUser.SMS}");
        }

        private void EnterMobilePhone(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.MobilePhone);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.MobilePhone)).SendKeys(newUser.MobilePhone);
            Reporter.LogPassingTestStepToBugLogger($"Put in the mobile phone into the phone input field. => {newUser.MobilePhone}");
        }

        private void EnterPostalAddress(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.PostalAdress);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.PostalAdress)).SendKeys(newUser.PostalAddress);
            Reporter.LogPassingTestStepToBugLogger($"Put in the postal address into the postal input area. => {newUser.PostalAddress}");
        }

        private void EnterPassword(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.Password);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.Password)).SendKeys(newUser.Password);
            Reporter.LogPassingTestStepToBugLogger($"Put in the password into the password field. => {newUser.Password}");
        }

        private void SelectStatus(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Dropdown.Status);
            DropdownSelect(Elements.CreatePage.Dropdown.Status, Elements.CreatePage.Dropdown.Active);
            Reporter.LogPassingTestStepToBugLogger($"Put in the account status in the staus filed useing the dropdown selector. => {newUser.Status}");
        }

        private void EnterUserName(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.UserName);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.UserName)).SendKeys(newUser.UserName);
            Reporter.LogPassingTestStepToBugLogger($"Put in the user name in the user name input field. => {newUser.UserName}");
        }

        private void EnterLocation(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.Location);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.Location)).SendKeys(newUser.ClientLocation);
            Reporter.LogPassingTestStepToBugLogger($"Put in the Client Location in the Location input field. => {newUser.ClientLocation}");
        }

        private void EnterClientName(Users.CurrentUser newUser)
        {
            WaitForElement(5, Elements.CreatePage.Input.Name);
            Driver.FindElement(By.XPath(Elements.CreatePage.Input.Name)).SendKeys(newUser.ClientName);
            Reporter.LogPassingTestStepToBugLogger($"Put in the name of the client in the name input field. => {newUser.ClientName}");
        }
    }
}
