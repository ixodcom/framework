﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;

namespace AutomationResources
{
    public class WebDriverFactory
    {
        public IWebDriver Create(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.Chrome:
                    return GetChromeDriver();
                default:
                    throw new ArgumentOutOfRangeException($"No such browser exists");
            }
        }
        private IWebDriver GetChromeDriver()
        {
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (outPutDirectory != null)
            {                
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("--disable-notifications");
                return new ChromeDriver(outPutDirectory, options);
            }
            else
            {
                throw new ArgumentOutOfRangeException($"Driver was not created and something went wrong");
            }
        }

        private string GetSeleniumBinaryLocation()
        {
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return Path.GetFullPath(Path.Combine(outPutDirectory, @"..\..\..\AutomationResources\bin\Debug"));
        }
    }
}
