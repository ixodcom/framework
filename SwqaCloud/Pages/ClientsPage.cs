﻿using System;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace SwqaCloud.Pages
{
    class ClientsPage : BaseApplicationPage
    {
        public RunEnvironment RunEnv { get; } = new RunEnvironment();

        public bool IsClientsPageLoaded
        {
            get
            {
                var isLoaded = Driver.FindElement(By.XPath(Elements.ClientsPage.PanelHeader.CrumbName)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the Clients Page is loaded");
                Logger.Trace($"Clients page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        public ClientsPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            RunEnv.Url = user.RunEnviroment == "PROD" ? Url.Production.LoginPage : Url.Test.LoginPage;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        internal void ClickAddButton()
        {
            WaitForElement(5, Elements.ClientsPage.Button.AddNew);
            var AddNewButton = Driver.FindElement(By.XPath(Elements.ClientsPage.Button.AddNew));
            AddNewButton.Click();
        }

        internal void ValidateClientsPageLoad()
        {
            WaitForElement(5, Elements.ClientsPage.PanelHeader.CrumbName);
            Assert.IsTrue(IsClientsPageLoaded, ErrorString.ClientsPageNotLoaded);
        }
    }
}
