﻿using System;
using NLog;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;

namespace SwqaCloud
{
    public class ScreenCapture
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IWebDriver _driver;
        private readonly TestContext _testContext;

        public ScreenCapture(IWebDriver driver, TestContext testContext)
        {
            if (driver == null)
                return;
            _driver = driver;
            _testContext = testContext;
            _screenCaptureFileName = _testContext.Test.Name;
        }

        public string ScreenCaptureFilePath { get; private set; }
        private string _screenCaptureFileName;

        public void CreateScreenCaptureIfTestFailed()
        {
            if (_testContext.Result.Outcome.Status == TestStatus.Failed ||
                _testContext.Result.Outcome.Status == TestStatus.Inconclusive)
                TakeScreenCaptureForFailure();
        }

        public string TakeScreenCapture(string screenCaptureFileName)
        {
            var screenCapture = GetScreenCapture();
            var successfullySaved = TryToSaveScreenCapture(screenCaptureFileName, screenCapture);

            return successfullySaved ? ScreenCaptureFilePath : "";
        }

        public bool TakeScreenCaptureForFailure()
        {
            _screenCaptureFileName = $"FAIL_{_screenCaptureFileName}";

            var screenCapture = GetScreenCapture();
            var successfullySaved = TryToSaveScreenCapture(_screenCaptureFileName, screenCapture);
            if (successfullySaved)
                Logger.Error($"Screenshot Of Error=>{ScreenCaptureFilePath}");
            return successfullySaved;
        }

        private Screenshot GetScreenCapture()
        {
            return ((ITakesScreenshot)_driver)?.GetScreenshot();
        }

        private bool TryToSaveScreenCapture(string screenshotFileName, Screenshot screenCapture)
        {
            try
            {
                SaveScreenshot(screenshotFileName, screenCapture);
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e.InnerException);
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);
                return false;
            }
        }

        private void SaveScreenshot(string screenshotName, Screenshot screenCapture)
        {
            if (screenCapture == null)
                return;
            ScreenCaptureFilePath = $"{Reporter.LatestResultsReportFolder}\\{screenshotName}.jpg";
            ScreenCaptureFilePath = ScreenCaptureFilePath.Replace('/', ' ').Replace('"', ' ');
            screenCapture.SaveAsFile(ScreenCaptureFilePath, ScreenshotImageFormat.Png);
        }
    }
}