﻿namespace SwqaCloud
{
    class Url
    {
        internal class Test
        {
            public const string LoginPage = "http://btc-dev-incbizco.s3-website-us-east-1.amazonaws.com/";
            public const string AdminPage = "http://btc-dev-incbizco.s3-website-us-east-1.amazonaws.com/admin";
        }
        internal class Production
        {
            public const string LoginPage = "http://swqa.cloud/";
            public const string AdminPage = "http://swqa.cloud/admin";
        }
    }
}
