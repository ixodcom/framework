﻿using System;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace SwqaCloud.Pages
{
    class AdminPage : BaseApplicationPage
    {
        public RunEnvironment RunEnv { get; } = new RunEnvironment();

        public AdminPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            RunEnv.Url = user.RunEnviroment == "PROD" ? Url.Production.LoginPage : Url.Test.LoginPage;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsLoginSuccessful
        {
            get
            {
                var isLoaded = Driver.FindElement(By.XPath(Elements.AdminPage.UserIcon.Name)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate user is logged into the Admin page.");
                Logger.Trace($"Clients Page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }

        internal void ClickClientMenuItem()
        {
            WaitForElement(5, Elements.SideMenu.Item.Client);
            var ClientMenuItem = Driver.FindElement(By.XPath(Elements.SideMenu.Item.Client));
            ClientMenuItem.Click();
        }

        internal void ValidateUserLogin(Users.CurrentUser user)
        {
            WaitForElement(15,Elements.AdminPage.UserIcon.Name);
            Assert.IsTrue(IsLoginSuccessful, ErrorString.AdminPageNotLoaded);
        }
    }
}
