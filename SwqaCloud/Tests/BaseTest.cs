﻿using System;
using AutomationResources;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace SwqaCloud.Tests
{
    public class BaseTest
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        protected IWebDriver Driver { get; private set; }
        public TestContext TestContext { get; set; }
        private ScreenCapture ScreenCapture { get; set; }

        [OneTimeSetUp]
        public void SetupForEveryTestRun()
        {
            Reporter.StartReporter();
        }

        [SetUp]
        public void SetupForEverySingleTestMethod()
        {
            Logger.Debug("*************[ TEST STARTED ]*************");
            var testContext = TestContext.CurrentContext;
            Reporter.AddTestCaseMetadataToHtmlReport(testContext);
            var factory = new WebDriverFactory();
            Driver = factory.Create(BrowserType.Chrome);
            Driver.Manage().Window.Maximize();
            ScreenCapture = new ScreenCapture(Driver, testContext);
        }

        [TearDown]
        public void TearDownForEverySingleTestMethod()
        {
            var testContext = TestContext.CurrentContext;
            Logger.Debug(GetType().FullName + " started a method tear down");
            try
            {
                TakeScreenshotForTestFailure();
            }
            catch (Exception e)
            {
                Logger.Error(e.Source);
                Logger.Error(e.StackTrace);
                Logger.Error(e.InnerException);
                Logger.Error(e.Message);
            }
            finally
            {
                StopBrowser();
                Logger.Debug(testContext.Test.Name);
                Logger.Debug("*************[ TEST STOPPED ]*************\n");
            }
        }

        private void TakeScreenshotForTestFailure()
        {
            if (ScreenCapture != null)
            {
                ScreenCapture.CreateScreenCaptureIfTestFailed();
                Reporter.ReportTestOutcome(ScreenCapture.ScreenCaptureFilePath);
            }
            else
            {
                Reporter.ReportTestOutcome("");
            }
        }

        private void StopBrowser()
        {
            if (Driver == null)
                return;
            Driver.Quit();
            Driver = null;
            Logger.Trace("Browser stopped successfully.");
        }
    }
}