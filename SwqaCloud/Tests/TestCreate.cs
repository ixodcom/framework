﻿using SwqaCloud.Pages;
using NUnit.Framework;

namespace SwqaCloud.Tests
{

    partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("Client Suite")]
            [TestCase(TestName = "CreateClient_0000")]
            [Author("Pat Holman", "pat@ixod.com")]
            public void CreateClient_0000()
            {
                var user = Users.GetTestUser000();
                var newUser = Users.GetTestUser001();
                var loginPage = new LoginPage(Driver, user);
                var adminPage = new AdminPage(Driver, user);
                var createPage = new CreatePage(Driver, user);
                var clientsPage = new ClientsPage(Driver, user);

                loginPage.LogInUser(user);
                adminPage.ValidateUserLogin(user);
                adminPage.ClickClientMenuItem();
                clientsPage.ValidateClientsPageLoad();
                clientsPage.ClickAddButton();
                createPage.FillOutNewClientForm(newUser);

                // comment
            }
        }
    }
}