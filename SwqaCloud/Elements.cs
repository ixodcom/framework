﻿namespace SwqaCloud
{
    class Elements
    {
        internal class LoginPage
        {
            internal class Input
            {
                public const string UserName = "//*[@id='username']";
                public const string Password = "//*[@id='password']";
            }
            internal class Button
            {
                public const string SignIn = "//*/button";
            }
        }
        internal class AdminPage
        {
            internal class UserIcon
            {
                public const string Name = "//*[@id='root']/div/div/section/div/header/ul/li/div";
            }
        }

        internal class SideMenu
        {
            internal class Item
            {
                public const string Client = "//a[contains(@href, 'admin/clients')]";
            }
        }
        internal class ClientsPage
        {
            internal class PanelHeader
            {
                public const string CrumbName = "//h3[text()='Clients']";
            }
            internal class Button
            {
                public const string AddNew = "//i[contains(@class,'plus')]/..";
            }
        }
        internal class CreatePage
        {
            internal class Input
            {
                //*[@id="name"]
                public const string Name = "//*[@id='name']"; // this is the name of the client company e.g. Acme Software
                public const string Location = "//*[@id='location']";
                public const string UserName = "//*[@id='owner.username']";
                public const string Password = "//*[@id='owner.password']";
                public const string PostalAdress = "//*[@id='owner.contactInformation.postalAddress']";
                public const string MobilePhone = "//*[@id='owner.contactInformation.mobilePhone']";
                public const string SMS = "//*[@id='owner.contactInformation.smsPhone']";
                public const string Service_1 = "//div[contains(@id,'instantMessengerInfos[0]')]";
                public const string Service_2 = "//div[contains(@id,'instantMessengerInfos[1]')]";
                public const string MessengerId_1 = "//input[contains(@id, 'owner.instantMessengerInfos[0]')]";
                public const string MessengerId_2 = "//input[contains(@id, 'owner.instantMessengerInfos[1]')]";
                public const string EmailAddress = "//*[@id='owner.contactInformation.emailAddress']";
                public const string LinkedInURL = "//*[@id='owner.contactInformation.linkedInUrl']";
                public const string ResumeURL = "//*[@id='owner.resumeUrl']";
            }

            internal class Button
            {
                public const string Submit = "//*[@id='btnSubmit']";
            }

            internal class Dropdown
            {
                public const string Status = "//div[contains(text(),'Status')]";
                public const string Active = "//ul/li[contains(text(),'Active')]";
                public const string Inactive = "//ul/li[contains(text(),'Inactive')]";
                public const string IMSelect_1 = "//div[contains(@id, 'owner.instantMessengerInfos[0]')]";            
                public const string IMSelect_2 = "//div[contains(@id, 'owner.instantMessengerInfos[1]')]";
                public const string Skype_1 = "//div[3]/div/div/div/ul/li[contains(text(),'Skype')]";
                public const string Viber_2 = "//div[4]/div/div/div/ul/li[contains(text(),'Viber')]";
            }      
        }
    }
}
