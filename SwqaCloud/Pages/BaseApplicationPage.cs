﻿using System;
using System.Threading;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace SwqaCloud.Pages
{
    public class BaseApplicationPage
    {
        protected IWebDriver Driver { get; set; }
        public string TestSystem { get; set; }

        public BaseApplicationPage(IWebDriver driver)
        {
            Driver = driver;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void WaitForElement(int seconds, string elementString)
        {
            Logger.Trace($"WAITING FOR ELEMENT: {elementString}");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(seconds));
            var element = By.XPath(elementString);
            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(element));
            }
            catch (Exception e)
            {
                Logger.Error($"ERROR: Element was not found {elementString}");
                Logger.Error(e);
                throw;
            }

            Logger.Trace($"ELEMENT FOUND: '{elementString}' <grin>");
        }

        public void ExplicitWait(int seconds)
        {
            int WaitTime = seconds * 1000;
            Thread.Sleep(WaitTime);
        }

        public void DropdownSelect(string dropdown, string selectItem)
        {
            Driver.FindElement(By.XPath(dropdown)).Click();
            ExplicitWait(1);
            Driver.FindElement(By.XPath(selectItem)).Click();
        }
    }

    public class RunEnvironment
    {
        public string Url { get; set; }
    }
}
