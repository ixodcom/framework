﻿using System;
using AventStack.ExtentReports;
using NLog;
using NUnit.Framework;
using OpenQA.Selenium;

namespace SwqaCloud.Pages
{
    class LoginPage : BaseApplicationPage
    {
        public RunEnvironment RunEnv { get; } = new RunEnvironment();
        
        public LoginPage(IWebDriver driver, Users.CurrentUser user) : base(driver)
        {
            RunEnv.Url = user.RunEnviroment == "PROD" ? Url.Production.LoginPage : Url.Test.LoginPage;
        }

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsLoginPageLoaded
        {
            get
            {
                var isLoaded = Driver.FindElement(By.XPath(Elements.LoginPage.Input.Password)).Displayed;
                Reporter.LogTestStepForBugLogger(Status.Info, "Validate the LoginPageLoaded Page is loaded");
                Logger.Trace($"Clients page is loaded=>{isLoaded}");
                return isLoaded;
            }
        }
        internal void LogInUser(Users.CurrentUser user)
        {
            GoToTheHomePage();
            EnterTheAppUserLoginName(user);
            EnterThePasswordForTheAppUser(user);
            ClickSignInButton();
        }

        private void ClickSignInButton()
        {
            WaitForElement(15, Elements.LoginPage.Button.SignIn);
            Driver.FindElement(By.XPath(Elements.LoginPage.Button.SignIn)).Click();
            Reporter.LogPassingTestStepToBugLogger("Click the sign in button ");
        }

        private void EnterThePasswordForTheAppUser(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.LoginPage.Input.Password);
            Driver.FindElement(By.XPath(Elements.LoginPage.Input.Password)).SendKeys(user.Password);
            Reporter.LogPassingTestStepToBugLogger("Enter a valid password in the password input field. ");
        }

        private void EnterTheAppUserLoginName(Users.CurrentUser user)
        {
            WaitForElement(5, Elements.LoginPage.Input.UserName);
            Driver.FindElement(By.XPath(Elements.LoginPage.Input.UserName)).SendKeys(user.UserName);
            Reporter.LogPassingTestStepToBugLogger("Enter a valid user name in the UserName input field. ");
        }

        private void GoToTheHomePage()
        {
            Driver.Navigate().GoToUrl(Url.Test.LoginPage);
            Assert.IsTrue(IsLoginPageLoaded, ErrorString.HomePageNotLoaded);
            Reporter.LogPassingTestStepToBugLogger("Navigated to Home Page loaded successfully ");
        }
    }
}
