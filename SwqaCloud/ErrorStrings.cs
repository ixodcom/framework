﻿namespace SwqaCloud
{
    class ErrorString
    {
        public const string HomePageNotLoaded = "Home page did not open successfully";
        public const string AdminPageNotLoaded = "The Admin Page did not load";
        public const string ClientsPageNotLoaded = "The Clients Page did not load correctly";
    }
}
