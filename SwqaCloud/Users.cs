﻿namespace SwqaCloud
{
    public class Users
    {
        internal static CurrentUser User { get; private set; }

        public static CurrentUser GetTestUser000()
        {
            User = new CurrentUser
            {
                UserName = "ixod321",
                Password = "password123"
            };
            return User;
        }
        public static CurrentUser GetTestUser001()
        {
            User = new CurrentUser
            {
                RunEnviroment = "TEST",
                ClientName = "Acme Software",
                ClientLocation = "Souix City IA",
                UserName = "RaviRajeshAditya",
                Password = "YourEyesOnly",
                Status = "Active",
                PostalAddress = "Gordon Dr, Sioux City, IA 51106",
                EmailAddress = "rra@example.com",
                MobilePhone = "(712) 252-0522",
                SMS = "(712) 252-5220",
                IM_01_Service = "Skype",
                MessengerId_1 = "SubbaMan",
                IM_02_Service = "WeChat",
                MessengerId_2 = "ManSubba",
                LinkedInURL = "https://www.linkedin.com/company/novell/",
                ResumeURL = "https://www.resume.com/user/EngieerMan"
            };
            return User;
        }

        public static CurrentUser GetTestUser002()
        {
            User = new CurrentUser
            {
                RunEnviroment = "TEST",
                ClientName = "Beta Software",
                ClientLocation = "Lake City CA",
                UserName = "PatJanHolman",
                Password = "CoolWords",
                Status = "Active",
                PostalAddress = "500 N 800 E, Lake Ciy, CA 91106",
                EmailAddress = "pj@example.com",
                MobilePhone = "(901) 252-0522",
                SMS = "(800) 252-5220",
                IM_01_Service = "Skype",
                MessengerId_1 = "Holman",
                IM_02_Service = "WeChat",
                MessengerId_2 = "FastDriver",
                LinkedInURL = "https://www.linkedin.com/user/PatJan/",
                ResumeURL = "https://www.resume.com/user/janpat"
            };
            return User;
        }


        public class CurrentUser
        {
            public string RunEnviroment { get; set; }
            public string ClientName { get; set; }
            public string ClientLocation { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public string Status { get; set; }
            public string PostalAddress { get; set; }
            public string EmailAddress { get; set; }
            public string MobilePhone { get; set; }
            public string SMS { get; set; }
            public string IM_01_Service { get; set; }
            public string MessengerId_1 { get; set; }
            public string IM_02_Service { get; set; }
            public string MessengerId_2 { get; set; }
            public string LinkedInURL { get; set; }
            public string ResumeURL { get; set; }
            public string HomeTown { get; set; }

        }
    }
}
