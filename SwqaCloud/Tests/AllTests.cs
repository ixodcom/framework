﻿using NUnit.Framework;
using SwqaCloud.Pages;

namespace SwqaCloud.Tests
{
    [TestFixture]
    internal partial class AllTests
    {
        private partial class AllTestsCases : BaseTest
        {
            //All other partial classes are part of this class
        }
    }
}
