﻿using SwqaCloud.Pages;
using NUnit.Framework;

namespace SwqaCloud.Tests
{

    partial class AllTests
    {
        private partial class AllTestsCases
        {
            [Test]
            [Category("Test Pat")]
            [TestCase(TestName = "LogInPat_000")]
            [Author("Pat Holman", "pat@ixod.com")]
            public void LogInPat_000()
            {
                var user = Users.GetTestUser000();
                var loginPage = new LoginPage(Driver, user);
                var adminPage = new AdminPage(Driver, user);
                loginPage.LogInUser(user);
                adminPage.ValidateUserLogin(user);
            }
        }
    }
}